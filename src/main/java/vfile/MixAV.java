package vfile;

import com.arcsoft.face.FaceInfo;
import net.coobird.thumbnailator.Thumbnails;
import org.bytedeco.javacpp.indexer.ByteIndexer;
import org.bytedeco.javacpp.indexer.UByteBufferIndexer;
import org.bytedeco.javacv.*;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.IplImage;
import org.springframework.util.CollectionUtils;
import util.WorkId;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

import static org.bytedeco.ffmpeg.global.avcodec.AV_CODEC_ID_AAC;
import static org.bytedeco.ffmpeg.global.avcodec.AV_CODEC_ID_H264;
import static org.bytedeco.ffmpeg.global.avutil.AV_PIX_FMT_YUV420P;

public class MixAV {
    static boolean exit = false;
    public static String savePath = "F:/face/";
    public static void main(String[] args) throws Exception {
        System.out.println("start...");
        String rtmpPath = savePath+"vido/1.avi";
        String rtspPath = savePath+"vido/file2.flv";
        boolean saveVideo = false;
        push(rtmpPath,rtspPath, saveVideo);
        System.out.println("end...");
    }

    public static void push(String rtmpPath, String rtspPath, boolean saveVideo) throws Exception {
        // 使用rtsp的时候需要使用 FFmpegFrameGrabber，不能再用 FrameGrabber
        File vf = new File(rtmpPath);
        FFmpegFrameGrabber grabberI = FFmpegFrameGrabber.createDefault(vf);
        grabberI.start();
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(rtspPath, grabberI.getImageWidth(), grabberI.getImageHeight(), 2);
        recorder.setVideoCodec(AV_CODEC_ID_H264);
        // 音频编/解码器
        recorder.setAudioCodec(AV_CODEC_ID_AAC);
        // rtmp的类型
        recorder.setFormat("flv");
        recorder.setPixelFormat(AV_PIX_FMT_YUV420P);
        recorder.start();
        //
        OpenCVFrameConverter.ToIplImage conveter = new OpenCVFrameConverter.ToIplImage();
        Java2DFrameConverter converter = new Java2DFrameConverter();
        System.out.println("all start!");
        int count = 0;
        //视频+音频
        //Frame frame = grabber.grab();
        //视频图片
        Frame frameI = null;
        //音频 grabber.grabSamples()
        Frame frameA = null;
        // 帧总数
        BufferedImage bImg = null;
        System.out.println("总时长:"+grabberI.getLengthInTime()/1000/60);
        System.out.println("总音频长:"+grabberI.getLengthInAudioFrames());
        System.out.println("总视频长:"+grabberI.getLengthInVideoFrames());
        System.out.println("总贞长:"+grabberI.getLengthInFrames());
        int frame_number = grabberI.getLengthInAudioFrames() + grabberI.getLengthInVideoFrames();
        long time = System.currentTimeMillis();
        int width = grabberI.getImageWidth();
        int height = grabberI.getImageHeight();
        int depth = 0;
        int channels = 0;
        int stride = 0;
        int pixelFormat = 0;
        for (int i = 1; i < frame_number; i++) {
            Frame frame1 = grabberI.grab();
            if(frame1 == null){
                continue;
            }
            Buffer[] smples = frame1.samples;
            if (smples != null) {
                recorder.recordSamples(smples);
            }
            Buffer[] imgs = frame1.image;
            if (imgs != null) {
                String frame2 = savePath+"1/4m.jpg";
                // 非常吃内存！！
                IplImage image =  opencv_imgcodecs.cvLoadImage(frame2);
                Frame frame3= conveter.convert(image);
                imgs = frame3.image;
                depth = frame3.imageDepth;
                channels = frame3.imageChannels;
                stride = frame3.imageStride;
                bImg = converter.convert(frame1);
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                Thumbnails.of(bImg).scale(1).outputFormat("jpg").toOutputStream(stream2);
                List<FaceInfo> code = FaceEngineUtils.checkFace(stream2.toByteArray());
                if(!CollectionUtils.isEmpty(code)){
                    Thumbnails.of(bImg).scale(1).outputFormat("jpg").toFile(savePath+"2/"+WorkId.sortUID());
                }
                stream2.close();

                recorder.recordImage(width, height, depth, channels, stride, -1, imgs);
                recorder.setTimestamp(frame1.timestamp);
                opencv_core.cvReleaseImage(image);
            }
        }
        grabberI.release();
        recorder.stop();
        recorder.release();
    }
}
